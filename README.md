# chopperAlarm conda recipe

Home: "https://gitlab.esss.lu.se/icshwi/nss-instruments/chopperalarm/chopperAlarm"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary:  Parses alarm output from neutron choppers
