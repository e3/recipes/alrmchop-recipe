#!/bin/bash

LIBVERSION=${PKG_VERSION}

# Clean between variants builds
make -f Makefile.E3 clean
make -f Makefile.E3 MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION}
make -f Makefile.E3 MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION} db
make -f Makefile.E3 MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION} install 

#install -m 644 alarms/*.iocsh  ${PREFIX}/modules/${PKG_NAME}/${PKG_VERSION}/
#install -m 644 alarms/*.db  ${PREFIX}/modules/${PKG_NAME}/${PKG_VERSION}/

